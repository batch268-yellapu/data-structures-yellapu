# 2
r = int(input(f'Enter the number of rows --> '))
c = int(input(f'Enter the number of columns --> '))

# 3
from numpy import *
matrixOne = [[0 for _ in range(c)] for _ in range(r)]
matrixTwo = [[0 for _ in range(c)] for _ in range(r)]
matrixSum = [[0 for _ in range(c)] for _ in range(r)]

#4
print("Input the values for First matrix")
for i in range(r):
	for j in range(c):
		matrixOne[i][j] = int(input(f'matrixOne[{i}][{j}]= '))

print("Input the values for Second matrix")
for i in range(r):
	for j in range(c):
		matrixTwo[i][j] = int(input(f'matrixTwo[{i}][{j}]= '))


print("******Display Matrix 1******")
for i in range (r):
	print(matrixOne[i])

print("******Display Matrix 2******")
for i in range (r):
	print(matrixTwo[i])


# 5
for i in range(r):
	for j in range(c):
		matrixSum[i][j] = matrixOne[i][j] + matrixTwo[i][j]
print("Sum of Two matrix")
for i in range (r):
	print(matrixSum[i])