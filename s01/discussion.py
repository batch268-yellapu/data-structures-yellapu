# [SECTION] Objects in Python
# Everything in python is an object

# int
print(f'Integer data type: {isinstance(int, object)}')

#str
print(f'String data type: {isinstance(str, object)}')

#function
def sample():
	pass

print(f'Function: {isinstance(sample, object)}')


# [SECTION] Parts of an object
x = 619

print(id(x)) # Reference Count
print(type(x)) # Type
print(x) # Value


# [SECTION] Two Types of Objects

#IMMUTABLE OBJECTS
numA = 5

print(id(numA))

numA += 1 #Adding 1 to the current value and reassingning it to the variable

print(id(numA))

#MUTABLE OBJECTS
numbers = [1, 2, 3, 4, 5]

print('-------Before Modyfying-------')
print(numbers)
print(id(numbers))

numbers[0] += 1

print('-------After Modyfying-------')
print(numbers)
print(id(numbers))


# [SECTION Lists
# Sublist
hero = ["Sniper", 50, True]

skills = [
	hero, ["Shrapnel", "headshot", "take_aim"]
]

print(skills)

# Indexing through sublists
print(skills[0][1])
print(hero[1])

# Length
print(len(hero))

# Concatenate
tasks = ["use_skills", "help_civilian", "play-with_friends"]

new_list = hero + tasks
print(new_list)

# Membership
isExisting = "Shrapnel" in skills[1]
print(isExisting)

# NOTE: Lists in python are mutabe


# [SECTION] Tuples
grades_tuple = (75, 77, 79)
print(grades_tuple)
print(type(grades_tuple))

# Another way of crating a tuple
print(tuple("Python"))

# Nesting Tuples
album_songs = ("Riot!", ("Miracle", "Fencas", "Thats_What_You_Get"))
print(album_songs)

# Indexing with nested tuple
print(album_songs[1][2]);


# Getting index of a value in a tuple
print(album_songs.index("Riot!"))


# [SECTION] Dictionary
Player_one = {
	"username": "68connossieur",
	"role": "Mage",
	"level": 99,
	"team": "in_team"
}

print(Player_one)

# Changing values using keys
Player_one["level"] -= 5
print(Player_one)