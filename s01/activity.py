# 2a
numbers = [1, 2, 3, 4, 5]
print(f'list of numbers: {numbers}\n')

# 2b
meals = ['spam', 'Spam', 'SPAM!']
print(f'list of numbers: {meals}\n')


# 3
message_list= ['Hi!']

# 3a
message_list = message_list *4
print(f'Result of repeat operator: {message_list}\n')

# 3b
print('List literals and operations using "numbers" list:')

print(f'Length of the list: {len(numbers)}')

print(f'The last item was removed from list: {numbers.pop()}')

reversed_list = numbers[::-1]
print(f'The list in reversed order: {reversed_list}\n')

# 3c
print('List literals and operations using "meals" list:')
print(f'using indexing operation:{meals[2]}')
print(f'using negative indexing operation: {meals[-2]}')

meals[meals.index("Spam")] = "eggs"
print(f'Modified list: {meals}')

adding_item = ["bacon"]
Added_list= meals+ adding_item
print(f'Added a new item int he list: {Added_list}')

Added_list.sort()
print(f'Modified list after sort method: {Added_list}\n')


# 4
recipe = {
	"eggs": 3
}
print(f'Created recipe dictionary: \nrecipe = {recipe}\n')

# 4b
recipe["spam"] = 2;
recipe["ham"] = 1;
recipe["brunch"] = "bacon"
print(f'Modified recipe dictionary: \nrecipe = {recipe}\n')

# 4c
recipe["ham"] = ["grill", "bake", "fry"];
print(f'Updated the "ham" key value: \nrecipe = {recipe}\n')

# 4d
del recipe["eggs"]
print(f'Modified recipe after deleting "eggs" key: \nrecipe = {recipe}\n')


# 5
bob = {
	"name": {
		"first": "Bob",
		"last" : "smith"
	},
	"age": 42,
	"job": ["software", "writing"],
	"pay": (40000, 50000)

}
print(f'Given "bob" dictionary: \nbob = {bob}\n')

# 5a
print(f'Accessing the value of "name" key: {bob["name"]}')

# 5b
print(f'Accessing the value of "last" key: {bob["name"]["last"]}')

# 5c
print(f'Accessing the second value of "pay" key: {bob["pay"][1]}\n')


# 6
numeric = ("Twelve", 3.0, [11, 22, 33])
print(f'Given "numeric" tuple:\nnumeric = {numeric}\n')

# 6a
print(f'Accessing a tuple item: {numeric[1]}')

# 6b
print(f'Accessing a tuple item: {numeric[2][1]}')