
# 1. Write a python statements that will satisfy the following situations and requirements:

	# a. Create a "numbers" list that will perform the following operations:
		# items that should be in the list: 5,11,17,19,25,29,30,30,32,46,90

numbers = [5,11,17,19,25,29,30,30,32,46,90]

		# a.1. Find the total number of items in the list.
print(f'\n{len(numbers)}')

		# a.2. Find how many times the item "30" occurs in the list
print(numbers.count(30))

		# a.3. Remove the item 17 from the numbers list.
numbers.pop(2)
print(numbers)

	# b. Create a SumEvenOdd() function to find the sum of all "even" and "odd" items in the "numbers" list.
numbers.insert(2,17)
sumeven = 0
sumodd = 0
for number in numbers:
	if (number%2 == 0):
		sumeven += number
	else:
		sumodd += number
print("\nSum of all Even and Odd items in the numbers list:")
print(f'Sum of Even numbers: {sumeven}')
print(f'Sum of Odd numbers: {sumodd}')


	# c. Marco is a programmer working on a project related with stack data structure. He is currently working with the functionality of storing visitors' name by implementing PUSH and POP method. But both of the functionalities are producing incorrect result. Help Marco on debugging his code by writing the correct code for the specific line number:

visitors = []

def PUSH(name):
	visitors.append(name) 
	top = len(visitors)-1

def POP(name):
	if len(visitors) == 0:
		return "Sorry! No Visitor to delete!"
	else:
		val = visitors.pop(0) 
		if len(visitors) == 0: 
			top = None 
		else:
			top == len(visitors) - 1
		return val



print(f"\nCurrent content of visitors: {visitors}")
print(f"Current length of the visitors: {len(visitors)}")
print("Invoking the POP method:")

print(POP(visitors))
PUSH("John")

print(f"\nUpdated visitors list after using PUSH('John') Method: {visitors}")
print(f"Current length of visitors list after using PUSH('John') Method: {len(visitors)}")
print("Invoking the POP method:")
print(POP(visitors))
print("Invoking the POP method again:")
print(POP(visitors))
print(f"Updated visitors list after using POP() Method: {visitors}")

	# d. Miah is a python programmer working on queue data structures. She implemented the ENQUEUE and DEQUEUE methods for managing the data base on the REQUESTNO. But both of the functionalities are not producing the desired results. Help Miah on debugging his code by writing the correct code for the specific line number:

requestsNo = []

def enqueue(item):
	requestsNo.append(item) 
	if len(requestsNo) == 0: 
		front == rear == 0
	else:
		rear = len(requestsNo)-1

def dequeue():
	if len(requestsNo) == 0:
		return "Underflow"
	else:
		val = requestsNo.pop()
		if len(requestsNo) == 0:
			front = rear = None 
		return val

print(f"\nCurrent content of requestsNo: {requestsNo}")
print(f"Current length of the requestsNo: {len(requestsNo)}")
print("Invoking the dequeue() method: ")
print(dequeue())

enqueue("RQN001")
enqueue("RQN002")

print(f"\nCurrent content of requestsNo after enqueue() method: {requestsNo}")
print(f"Current length of the requestsNo after enqueue()method : {len(requestsNo)}")
print("Invoking the dequeue() method: ")
print(dequeue())
print(f"Current content of requestsNo after dequeue() method: {requestsNo}\n")

	# e. Using the provided dictionary below, create lists of its keys, values and items and show those lists.

roman_numerals = {'I': 1, 'II': 2, 'III': 3, 'V': 5}

		# e.1. Print the keys of the "roman_numerals" dictionary
keys = list(roman_numerals.keys())
print(keys)

		# e.2. Print the values of the "roman_numerals" dictionary
values = list(roman_numerals.values())
print(values)

		# e.3. Print the items of the "roman_numerals" dictionary
items = list(roman_numerals.items())
print(items)

# 4. Create a git repository named s01 inside your provided GitLab Subfolder.

# 5. Initialize a git repository, stage the files in preparation for commit, create commit with the message Add Activity Code and push changes to the remote repository.